package com.example.dailybudgetcalculator

import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.util.Log
import android.widget.EditText
import android.widget.TextView
import androidx.annotation.RequiresApi
import java.time.LocalDate
import java.time.Period

@RequiresApi(26)
class MainActivity : AppCompatActivity() {
    private var _bankBalance: Double = 0.0
    private var _nextPayday: LocalDate? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        _nextPayday = getNexDefaultPayday()
        val nextPaydayEdit: EditText = findViewById<EditText>(R.id.payday_edit)
        nextPaydayEdit.setText(_nextPayday.toString())

        // OnClick listener for calculate button
        findViewById<Button>(R.id.calculate_button).setOnClickListener {
            setBankBalance()
            setNextPayday()
            findViewById<TextView>(R.id.daily_budget_display).text = getDailyBudgetStr()
        }
    }

    private fun calculateDailyBudget(): Double? {
        log("calculating daily budget...")
        val numberOfDays: Int = getNumberOfDaysUntilPayday()
        log("Number of days until next payday: %d".format(numberOfDays))
        return _bankBalance / numberOfDays
    }

    private fun getDailyBudgetStr(): String {
        val dailyBudgetDouble = calculateDailyBudget() ?: return ""
        return String.format("%.2f", dailyBudgetDouble)
    }

    private fun getBankBalanceFromUI(): Double {
        var bankBalanceStr = findViewById<EditText>(R.id.bank_balance_edit).text.toString().replace(",", ".")
        if (bankBalanceStr == ""){
            bankBalanceStr = "0.0"
        }
        return try {
            String.format("%.2f", bankBalanceStr.toDouble()).toDouble()
        } catch (e: java.lang.NumberFormatException){
            0.0
        }
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getNexDefaultPayday(): LocalDate {
        var newPayday = LocalDate.now().plusDays(1)
        while (newPayday.dayOfMonth != 15) {
            newPayday = newPayday.plusDays(1)
        }
        while (newPayday.dayOfWeek == java.time.DayOfWeek.SUNDAY || newPayday.dayOfWeek == java.time.DayOfWeek.SATURDAY){
            newPayday = newPayday.minusDays(1)
        }
        return newPayday
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun getNextPaydayFromUI(): LocalDate? {
        var nextPayday: String = findViewById<EditText>(R.id.payday_edit).text.toString()
        if (nextPayday == ""){
            nextPayday = getNexDefaultPayday().toString()
            updateNextPayday2UI(LocalDate.parse(nextPayday))
        }
        var nextPayDayDate: LocalDate? = null
        try {
            nextPayDayDate = LocalDate.parse(nextPayday)
        } catch (e: java.time.format.DateTimeParseException){
            nextPayDayDate = getNexDefaultPayday()
            updateNextPayday2UI(nextPayDayDate)
        } finally {
            return nextPayDayDate
        }
    }

    private fun getNumberOfDaysUntilPayday(): Int {
        return Period.between(LocalDate.now(), _nextPayday).days
    }

    private fun log(msg: String) {
        Log.i("DailyBudgetCalculator", msg)
    }

    private fun setBankBalance() {
        val bankBalanceFromUI = getBankBalanceFromUI()
        _bankBalance = bankBalanceFromUI
        log("Bank balance set to %.2f".format(_bankBalance))
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun setNextPayday() {
        val nextPayday = getNextPaydayFromUI()
        if (nextPayday != null){
            _nextPayday = nextPayday
        }
    }

    private fun updateNextPayday2UI(nextPayday: LocalDate){
        val nextPaydayEdit: EditText = findViewById<EditText>(R.id.payday_edit)
        nextPaydayEdit.setText(nextPayday.toString())
    }
}